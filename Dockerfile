# Use a base image such as jupyter/base-notebook
FROM jupyter/base-notebook

# Install Jupyter notebook using the appropriate package manager
RUN pip install jupyter

# Expose the port using the EXPOSE directive in the Dockerfile
EXPOSE 3000

# Switch to root user to install sudo and grant sudo privileges
USER root
RUN apt-get update && apt-get install -y sudo && usermod -aG sudo jovyan
USER jovyan

# Set the IP and port for the Jupyter notebook using the appropriate command or configuration
CMD ["jupyter", "notebook", "--ip=0.0.0.0", "--port=3000"]
